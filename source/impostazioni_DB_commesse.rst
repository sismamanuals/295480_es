.. _impostazioni_DB_commesse:

########################
IMPOSTAZIONI DB COMMESSE
########################
*******************************************
Funzionalità di connessione database PRISMA
*******************************************
Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > DB Commesse` permette di connettersi a un database esterno e caricare automaticamente i progetti relativi a un risultato della query.

PROPRIETÀ DB LAVORI
===================
#. Attivare l'opzione spuntando la casella indicata al punto 1 della :numref:`impostazioni_applicazione_DBcommesse_abilitazione`;
#. Selezionare la cartella di progetto selezionando il pulsante indicato al punto 2 della :numref:`impostazioni_applicazione_DBcommesse_abilitazione`;
#. Impostare la stringa di connessione del database nella finestra che apparirà (vedere :numref:`impostazioni_applicazione_DBcommesse_connessioneDB`).

.. _impostazioni_applicazione_DBcommesse_abilitazione:
.. figure:: _static/impostazioni_applicazione_DBcommesse_abilitazione.png
   :width: 14 cm
   :align: center

   Schermata DB Lavori

.. _impostazioni_applicazione_DBcommesse_connessioneDB:
.. figure:: _static/impostazioni_applicazione_DBcommesse_connessioneDB.png
   :width: 10 cm
   :align: center

   Schermata Connessione DB

I parametri della schermata in :numref:`impostazioni_applicazione_DBcommesse_connessioneDB` sono:

* :kbd:`Nome Data source` nome del server dove il database è allocato;
* :kbd:`Nome Database` è il nome del database al quale connettersi;
* :kbd:`User ID` e :kbd:`Password` sono le credenziali con le quali accedere al database;
* :kbd:`DB Remoto` indica se il database è locale o meno.

La corretta impostazione dei parametri può essere verificata tramite il tasto :kbd:`Test`. Il colore dell'icona indica il risultato della verifica:

* *verde* significa che la connessione è andata a buon fine;
* *rossa* significa che la connessione non è andata a buon fine.

Nella casella :guilabel:`Select Query`, scrivere la query per ottenere risultati utilizzando la lingua standard di SQL Server. 

Questa query deve avere un parametro chiamato **@PARAM_ID** che verrà sostituito dal codice letto dalla finestra principale :guilabel:`Carica progetto da DB`.

**Code Reader Mask** è una maschera che può filtrare il codice letto, usando alcuni caratteri speciali:

* Usa il simbolo ``#`` per saltare un carattere;
* Usa ``*`` per leggere il carattere;
* Usa ``?`` per impostare l'ultimo carattere leggibile.

Se, per esempio, il codice è ``AQ7P1234DFTY9745FTCV`` e la maschera è ``####************?`` allora il risultato sarà ``1234DFTY9745``. Questo perché i primi 4 caratteri vengono saltati, così come quelli successivi al ``?``.

Il pulsante **Test Query** consente di verificare se la query scritta restituisce alcuni risultati. In caso il controllo sia negativo notificherà un errore.
La tabella degli elementi della query consente di abbinare le proprietà del progetto Prisma ai campi DB caricati dai risultati della query.

Abbinare:

* il **Tipo di Progetto** al campo della query del risultato che rappresenta il valore del nome del progetto;
* il **Tipo di Livello** con il campo che rappresenta il nome del file del livello;
* il **Tipo di Copie** con il numero di copie archiviato che rappresenta il numero di copie di lavoro;
* un elenco di **Variabili** con campi di query correlati da aggiornare quando il progetto viene caricato.

Al posto del nome della variabile è possibile impostare una funzione speciale per abilitare o disabilitare un piano di lavoro specifico o modificarne le coordinate.
Sintassi della funzione speciale: ``WPnF`` dove:

* ``WP``: richiama la funzionalità
* ``n``: indice del piano di lavoro
* ``F``: funzione da applicare

.. csv-table:: Tabella delle funzioni
   :header: "Funzione F", "Descrizione", "Valore DB"
   :align: center

   "E", "abilitato", "-1 not used, 0 disabled, 1 enabled"
   "X", "coordinata X", "Value, 1000000 do not move"
   "Y", "coordinata Y", "Value, 1000000 do not move"
   "Z", "coordinata Z", "Value, 1000000 do not move"

CARICAMENTO PROGETTO
====================
Per caricare un progetto cliccare sull'icona in :numref:`caricamento_progetto_DB`

.. _caricamento_progetto_DB:
.. figure:: _static/caricamento_progetto_DB.png
   :width: 10 cm
   :align: center

   Schermata Caricamento Progetto DB


La finestra :guilabel:`Caricamento Progetto da DB` si aprirà in attesa di un codice da leggere. Se il codice si riferirà ad un risultato nel database allora il software PRISMA verificherà il percorso del file di progetto e del file dei layer ed aprirà il relativo progetto.

.. _apertura_progetto_DB:
.. figure:: _static/apertura_progetto_DB.png
   :width: 14 cm
   :align: center

   Schermata Apertura Progetto DB

Se la richiesta ritorna un errore allora una finestra informerà l'utente.

.. _errore_apertura_progetto_DB:
.. figure:: _static/errore_apertura_progetto_DB.png
   :width: 14 cm
   :align: center
   
   Schermata Errore Apertura Progetto DB

CONFIGURAZIONE DB
=================
Per configurare un nuovo database:

* Connettersi al **Server Name** con autenticazione, se richiesta (vedere :numref:`connessione_server_name`);
* Creare un nuovo database (vedere :numref:`creazione_DB_1` e :numref:`creazione_DB_2`);
* Creare una nuova tabella (vedere :numref:`creazione_tabella_1` e :numref:`creazione_tabella_2`);
* Inserire o editare le righe (vedere :numref:`edit_insert_righe_1` e :numref:`edit_insert_righe_2`).

.. _connessione_server_name:
.. figure:: _static/connessione_server_name.png
   :width: 14 cm
   :align: center

   Connessione al **Server**


.. _creazione_DB_1:
.. figure:: _static/creazione_DB_1.png
   :width: 14 cm
   :align: center

   Creazione nuovo database - 1


.. _creazione_DB_2:
.. figure:: _static/creazione_DB_2.png
   :width: 14 cm
   :align: center

   Creazione nuovo database - 2


.. _creazione_tabella_1:
.. figure:: _static/creazione_tabella_1.png
   :width: 14 cm
   :align: center

   Creazione nuova tabella - 1


.. _creazione_tabella_2:
.. figure:: _static/creazione_tabella_2.png
   :width: 14 cm
   :align: center

   Creazione nuova tabella - 2


.. _edit_insert_righe_1:
.. figure:: _static/edit_insert_righe_1.png
   :width: 14 cm
   :align: center

   Creazione/modifica righe - 1


.. _edit_insert_righe_2:
.. figure:: _static/edit_insert_righe_2.png
   :width: 14 cm
   :align: center

   Creazione/modifica righe - 2
