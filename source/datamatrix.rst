﻿.. _datamatrix:

##################
CODICE DATA MATRIX
##################
Data Matrix è un codice a barre bidimensionale a matrice, composto da celle (o moduli) bianche e nere disposte all'interno di uno schema di forma rettangolare o quadrata.

*********************
CREAZIONE DATA MATRIX
*********************
L'attivazione si effettua selezionando la voce :guilabel:`Aggiungi Codice 2D` nella barra degli strumenti presenti nell'area di lavoro. 

.. _datamatrix_aggiungi_codice_2D:
.. figure:: _static/datamatrix_aggiungi_codice_2D.png
   :scale: 70 %
   :align: center

   Selezione *Aggiungi Codice 2D*

A questo punto apparirà sull'area di lavoro un codice standard, che si dovrà personalizzare nella finestra :guilabel:`Proprietà`.

Prima di eseguire la marcatura è necessario compilare le impostazioni descritte.

*********************
PROPRIETÀ DATA MATRIX
*********************
La proprietà del *data matrix* sono divise in due macro gruppi:

* Barcode (contiene le informazioni relative ai dati contenuti da codice)
* File Grafico (contiene le informazioni relative alla dimensione e alla posizione del codice nell'area di lavoro)

*********************************
IMPOSTAZIONE BASE DEL DATA MATRIX
*********************************
*****************
PROPRIETÀ BARCODE
*****************
Contiene le informazioni relative ai dati contenuti dal codice.

**Tipo**

   Seleziona il tipo di codice a barre che si desidera usare.
   Lasciare la selezione proposta di Default (*DataMatrix*).

**Data**

   Dati contenuti nel *DataMatrix*. Si possono includere sino a 50 caratteri.

**Colore**

   Definisce il colore del parametro laser che verrà usato per marcare il *DataMatrix*.

**Sfondo**

   Permette di inserire uno sfondo al *DataMatrix*. Si può inserire solo uno sfondo, che verrà marcato prima o dopo a seconda della posizione, nella finestra Livelli, dei colori scelti per matrix e sfondo.

**Formato**
   Lasciando *Default* il sistema sceglie in automatico la dimensione minima del *DataMatrix*.

**Negativo**

   Inverte il colore dello sfondo con il *DataMatrix* e viceversa.

**Bordo**

   Inserisce un bordo attorno al *DataMatrix* utile a migliorare la leggibilità in caso di presenza di uno sfondo.

PROPRIETÀ BARCODE - ESPERTO
===========================
Contiene ulteriori informazioni relative ai dati contenuti dal codice.

**Sequenza Escape**

   *Inserire descrizione*

**Bordo sopra**

   *Inserire descrizione*

**Bordo sotto**

   *Inserire descrizione*

**Bordo sinistro**

   *Inserire descrizione*

**Bordo destro**

   *Inserire descrizione*

PROPRIETÀ BARCODE - ESPERTO - MODULO
====================================
**Forma Modulo**

   *Inserire descrizione*

**Riduzione modulo**

   *Inserire descrizione*

PROPRIETÀ BARCODE - ESPERTO - DATA MATRIX INTERNO
=================================================
**Abilita**

   *Inserire descrizione*

**********************
PROPRIETÀ FILE GRAFICO
**********************
Contiene le informazioni relative alla dimensione e alla posizione del codice a barre nell'area di lavoro.

**Nome**

   *Inserire descrizione*

**Esterno**

   *Inserire descrizione*

**X** e **Y**

   Indica la posizione all'interno dell'area di lavoro del Barcode.

**Larghezza** e **Altezza**

   Definisce le dimensioni, in mm, del Barcode.

**Rotazione**

   Serve per introdurre un angolo di rotazione al codice a barre.

**Scala X** e **Scala Y**

   *Inserire descrizione*

**Spessore**

   *Inserire descrizione*

*********************
MARCATURA DATA MATRIX
*********************

* Settare l'altezza Z[mm] del punto di marcatura nella finestra :guilabel:`Struttura Progetto` e premere :kbd:`GO`.
* Selezionare i parametri di marcatura nella finestra :guilabel:`Livelli`.
* Premere :kbd:`ROSSO` per centrare l'area di marcatura sul pezzo.
* Chiudere lo sportello.
* Premere :kbd:`START` per eseguire la marcatura.
