﻿######################
IMPOSTAZIONI ACCESSORI
######################
Procedura per impostare i parametri di utilizzo degli accessori.

Il parametro :kbd:`MODALITÀ` della tab :kbd:`Struttura Progetto` consente di gestire tutti gli accessori del sistema laser.

Per poter utilizzare questo parametro:

#. Selezionare la voce :kbd:`Accessori Esterni` nella sezione :guilabel:`Interfaccia` che si trova in: :guilabel:`Strumenti > Impostazioni > Applicazione` (vedere :numref:`impostazioni_accessori_abilitazione`);
#. premere il comando :kbd:`APPLICA`;
#. Selezionare il menù :kbd:`Struttura Progetto` nella finestra :guilabel:`Struttura Progetto`;
#. Selezionare alla voce :kbd:`Modalità` il tipo di lavorazione desiderata tra:

   * *Planare*
   * *Mandrino* (vedere :numref:`impostazioni_mandrino`)
   * *Traino Lastra* (vedere :numref:`impostazioni_traino_lastra`)
   * *Traino Zip* (vedere :numref:`impostazioni_traino_zip`)

#. Selezionare il menù :kbd:`Accessori` nella tab :guilabel:`Struttura Progetto` per accedere alle impostazioni della selezione.

.. _impostazioni_accessori_abilitazione:
.. figure:: _static/impostazioni_accessori_abilitazione.png
   :width: 14 cm
   :align: center

   Abilitazione Accessori

.. _impostazioni_traino_lastra:

**************************************
IMPOSTAZIONI ACCESSORI - TRAINO LASTRA
**************************************
Nella tab :kbd:`Struttura Progetto` impostare il parametro :kbd:`MODALITÀ` selezionando :guilabel:`Traino Lastra` per poter procedere con l'uso del **Traino Lastra**.

.. _impostazioni_accessori_menù_traino_lastra:
.. figure:: _static/impostazioni_accessori_menù_traino_lastra.png
   :width: 14 cm
   :align: center

   Menù Accessori - Traino Lastra

Nella finestra :guilabel:`Accessori` si trovano i seguenti campi:

* :kbd:`Modalità` (se la dimensione del file supera la dimensione massima dell'area di marcatura si rende necessario partizionare l'immagine);
* :kbd:`Avanzamento` (viene attivata solo se di usa *Extra Fine* o *Massima Lunghezza* e viene richiesto di inserire la dimensione della sezione/avanzamento della partizione);
* :kbd:`Spaziatura` (indica lo spazio in mm che verrà messo tra l'esecuzione di un file ed il successivo);
* :kbd:`FILES` (contiene i comandi per gestire i file che verranno usati con il traino lastra; questa gestione differisce da quella dei comuni file gestiti nella :guilabel:`Struttura Progetto`).

******************
IMPOSTAZIONI FILES
******************
Per il **Traino Lastra** la gestione dei file avviene nella finestra :kbd:`FILES`.

I comandi sono:

* :kbd:`Aggiungi` (aggiunge un file alla lista da eseguire);
* :kbd:`Rimuovi` (rimuove il file evidenziato dalla lista da eseguire);
* :kbd:`Sposta giù` e :kbd:`Sposta su` (sposta il file evidenziato in alto o in basso nella lista da eseguire);
* :kbd:`Apri` e :kbd:`Chiudi` (apre o chiude le pinze del traino lastra per bloccare o rimuovere la lastra);
* :kbd:`Apri ad ogni Movimento` (attivando l'opzione si imposta affinché le pinze si aprano ad ogni cambio file)
* :kbd:`Rilascia` (toglie coppia al Traino Lastra, permettendo di spostare agevolmente la lastra);
* :kbd:`Copie` (indica quante copie dello stesso file si desidera eseguire).

**************************
IMPOSTAZIONE TRAINO LASTRA
**************************
Per procedere con la marcatura e/o taglio:

#. Posizionare il traino lastra all'interno della camera di lavoro, fissandolo al piano di lavoro usando le apposite predisposizioni;
#. Collegare l'apposito connettore del traino lastra alla spina presente nella camera di lavoro;
#. Inserire la lastra nel traino, regolando in larghezza le pinze, in modo che, alla loro chiusura, il blocco sia ottimale.
#. Aggiungere uno o più file (si posso attivare o disattivare i file che si desidera usare);
#. Se necessario, selezionare la :kbd:`Modalità` nella finestra  :guilabel:`PARTIZIONAMENTO` tra:

   * *Media*
   * *Pack*
   * *Fine*
   * *Extrafine*
   * *Massima Lunghezza*

   Si veda :ref:`Abilitazione accessori partizionamento <abilitazione_accessori_partizionamento>` per avere la descrizione in dettaglio delle diverse modalità.

#. Inserire il valore :kbd:`Avanzamento` (se editabile) per indicare la larghezza del settore da marcare;
#. Inserire il valore :kbd:`Spaziatura` se si desidera che il sistema aggiunga dello spazio tra un file ed il successivo;
#. Inserire il valore :kbd:`Altezza Z [mm]` del punto di marcatura nella finestra :guilabel:`Struttura Progetto` e premere :kbd:`GO`;
#. Selezionare i parametri di marcatura nella finestra :guilabel:`Livelli`;
#. Premere :kbd:`ROSSO` per centrare l'area di marcatura sulla lastra;
#. Chiudere lo sportello;
#. Premere :kbd:`START` per eseguire la marcatura.

.. NOTE::

   |notice| Salvare le impostazioni create per il **Traino Lastra**.
   Per fare questo riferirsi a :numref:`salvataggio_impostazioni` ( :ref:`salvataggio_impostazioni` ).

.. _impostazioni_mandrino:

*********************************
IMPOSTAZIONI ACCESSORI - MANDRINO
*********************************
Nella tab :kbd:`Struttura Progetto` impostare il parametro :kbd:`MODALITÀ` selezionando :guilabel:`Mandrino` per poter procedere con l'uso del **Mandrino**.

.. _impostazioni_accessori_menù_mandrino:
.. figure:: _static/impostazioni_accessori_menù_mandrino.png
   :width: 14 cm
   :align: center

   Menù Accessori - Mandrino

In questa modalità si lavora direttamente sullo sviluppo dell'anello, andando a posizionare tutto quello che si intende marcare direttamente sulla sua superficie.

Per fare questo è necessario compilare i dati della finestra :guilabel:`Accessori`.

Nella finestra :guilabel:`Accessori` si trovano le ulteriori finestre:

* :kbd:`POSIZIONE ANELLO` (viene definita la zona dell'anello che verrà marcata se interna o esterna rispetto la posizione del mandrino);
* :kbd:`DIMENSIONI` (contiene le informazioni necessarie alla creazione dello sviluppo dell'anello);
* :kbd:`PARTIZIONAMENTO` (indica la modalità con il quale deve essere suddivisa la lavorazione);
* :kbd:`COMANDI` (permettono di agire direttamente sul mandrino).

*****************************
IMPOSTAZIONE MARCATURA ANELLO
*****************************
Per procedere con la marcatura di un anello:

#. Posizionare il mandrino all'interno della camera di lavoro, fissandolo al piano di lavoro usando le apposite predisposizioni;
#. Collegare l'apposito connettore del mandrino alla spina presente nella camera di lavoro;
#. Inserire l'anello nelle pinze del mandrino e passare alla compilazione delle impostazioni di marcatura.
#. Nella finestra :guilabel:`Accessori`:

   * Selezionare la :kbd:`Posizione Anello` corretta tra:

      * mandrino a sinistra con marcatura esterna anello
      * mandrino a sinistra con marcatura interna anello 
      * mandrino frontale con marcatura esterna anello
      * mandrino frontale con marcatura interna anello
      * mandrino a destra con marcatura esterna anello
      * mandrino a destra con marcatura interna anello

   La barra dell'opzione selezionata diventa verde.

#. Selezionare il parametro :kbd:`Unità Angolare` (Gradi - mm - Inch - Passi);
#. Indicare o la :kbd:`Lunghezza` dello sviluppo dell'anello oppure il suo :kbd:`Diametro` in mm (l'area di lavoro mostrata sarà l'area di sviluppo dell'anello);
#. Importare o creare il file da marcare e posizionarlo nel punto di sviluppo dell'anello che si desidera marcare;
#. :kbd:`Angolo Finale` (se necessario indicare a quale grado di rotazione si deve poi posizionare il mandrino alla fine della lavorazione);
#. :kbd:`Inclinazione` (se l'anello è inclinato, indicando il grado di inclinazione, il software applica una deformazione al file così da compensare l'inclinazione);
#. Selezionare la Modalità di partizione tra:

   * *Media*
   * *Pack*
   * *Fine*
   * *Extrafine*
   * *Massima Lunghezza*
   * *Continua*

   (si consiglia di usare *Fine* in quanto è la modalità che ricopre la maggior parte delle casistiche).

   Si veda :ref:`Abilitazione accessori partizionamento <abilitazione_accessori_partizionamento>` per avere la descrizione in dettaglio delle diverse modalità.

#. Inserire il valore :kbd:`Avanzamento` (se editabile) per indicare la larghezza del settore da marcare;
#. Inserire il valore :kbd:`Linee/Settore` se si utilizza la :kbd:`Modalità`: *Continua*;
#. Settare eventuali impostazioni nella finestra :guilabel:`COMANDI` (se necessario);
#. Inserire il valore :kbd:`Altezza Z [mm]` del punto di marcatura nella finestra :guilabel:`Struttura Progetto` e premere :kbd:`GO`;
#. Selezionare i parametri di marcatura nella finestra :guilabel:`Livelli`;
#. Premere :kbd:`ROSSO` per centrare l'area di marcatura sull'anello.
#. Chiudere lo sportello;
#. Premere :kbd:`START` per eseguire la marcatura.

.. NOTE::

   |notice| Salvare le impostazioni create per il **Mandrino**.
   Per fare questo riferirsi a :numref:`salvataggio_impostazioni` ( :ref:`salvataggio_impostazioni` ).

.. _abilitazione_accessori_comandi:

.. NOTE::

   |notice|
   **SCHEDA DI APPROFONDIMENTO - Comandi Mandrino**

   La parte Comandi permette di agire direttamente sul mandrino.

   * :kbd:`Trova Zero`: ruota il mandrino fino alla sua posizione di zero;
   * :kbd:`Posizione`: indica la posizione alla quale si deve posizionare il mandrino; questa viene riferita rispetto alla posizione di zero. Se si immette un valore di 30° e si preme :kbd:`SET`, prima viene trovato lo zero e poi viene fatto uno spostamento di 30°;
   * :kbd:`Avanzamento`: è uno spostamento relativo riferito alla posizione attuale. Se si immette un valore di 30° e si preme :kbd:`SET`, il mandrino si sposta subito di questo valore, senza eseguire la ricerca dello zero;
   * :kbd:`Azzera`: esegue uno spostamento sullo zero prima di avviare la marcatura;
   * :kbd:`Linea guida`: in modalità di partizionamento, il mandrino ruota sempre garantendo che la linea guida diventi un punto di riferimento assoluto rispetto al file da marcare;
   * :kbd:`Rilascia`: toglie energia (coppia) al mandrino, permettendo di ruotare manualmente le pinze per portare l'anello nella posizione desiderata. Le pinze tornano in coppia al comando successivo.

.. _abilitazione_accessori_partizionamento:

.. NOTE::

   |notice|
   **SCHEDA DI APPROFONDIMENTO - Partizionamento/Modalità**

   La :kbd:`Modalità` nella tab :kbd:`Partizionamento` indica come deve essere suddivisa la lavorazione da eseguire.

   * :kbd:`Media`: tiene conto dell'unione o della combinazione dei vari elementi grafici (attenzione: unione o combinazione, non raggruppamento). Se sono uniti o combinati, li marca insieme;
   * :kbd:`Pack`: raccoglie in un'unica marcatura gli oggetti che si trovano all'interno del rettangolo di occupazione. Può essere utile, per esempio, quando si vuole che una targhetta venga marcata in una sola volta con tutti i suoi dati;
   * :kbd:`Fine`: vengono marcati singolarmente tutti gli elementi che non hanno continuità tra loro;
   * :kbd:`Extrafine`: si stabilisce la larghezza del settore da marcare e gli elementi vengono spezzati in corrispondenza dei limiti del settore, il valore viene immesso nel campo avanzamento; ideale quando gli elementi grafici sono di lunghezza tale da non consentire una marcatura a fuoco;
   * :kbd:`Lunghezza Massima`: impostata una misura, sul campo avanzamento, tutti i vettori più lunghi di questo valore verranno ridotti in segmenti al massimo di tale misura. La lunghezza ottimale sarà all'incirca la metà rispetto a quanto si usa per l'extrafine, in quanto i vettori possono sbordare a destra o a sinistra per metà della misura stessa;
   * **Solo per mandrino** :kbd:`Continua`: divide l'intera immagine da marcare in settori composti da singole linee orizzontali o da gruppi di linee orizzontali. In questa modalità le impostazioni di inclinazione del riempimento dei :guilabel:`Livelli` non vengono usate. Si richiede l'inserimento da parte dell'utente dei valori :kbd:`Avanzamento` e :kbd:`Linee/Settore`.

.. _impostazioni_traino_zip:

***********************************
IMPOSTAZIONI ACCESSORI - TRAINO ZIP
***********************************
Nella tab :kbd:`Struttura Progetto` impostare il parametro :kbd:`MODALITÀ` selezionando :guilabel:`Traino Zip` per poter procedere con l'uso del **Traino Zip**.

.. _impostazioni_accessori_menù_traino_zip:
.. figure:: _static/impostazioni_accessori_menù_traino_zip.png
   :width: 14 cm
   :align: center

   Menù Accessori - Traino Zip

Nella finestra :guilabel:`Accessori` si trovano i seguenti campi:

* :kbd:`Homing` *Inserire descrizione*
* :kbd:`Vai avanti` *Inserire descrizione*
* :kbd:`Apri` *Inserire descrizione*
* :kbd:`Chiudi` *Inserire descrizione*
* :kbd:`Accelerazione [rps2]` *Inserire descrizione*
* :kbd:`Decelerazione [rps2]` *Inserire descrizione*
* :kbd:`Velocità Homing [%]` *Inserire descrizione*
* :kbd:`Velocità [%]` *Inserire descrizione*
* :kbd:`Avanzamento denti` *Inserire descrizione*.
